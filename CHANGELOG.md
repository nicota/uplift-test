# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/), and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## Unreleased

## [v0.2.0](https://gitlab.com/nicota/uplift-test/-/tags/v0.2.0) - 2022-12-23

- [`d55ac7b`](https://gitlab.com/nicota/uplift-test/-/commit/d55ac7b8b42158e7f93adf36987873dabb173b95) ci: updated gitlab CI config
- [`cb424f4`](https://gitlab.com/nicota/uplift-test/-/commit/cb424f46419049209c376bc981c9fab1cc11bde6) ci: updated gitlab CI config
- [`6222dbb`](https://gitlab.com/nicota/uplift-test/-/commit/6222dbb499e58bdd7750610470820328f8c7a45f) ci: updated gitlab CI config
- [`0d41771`](https://gitlab.com/nicota/uplift-test/-/commit/0d4177131cee998e7c68887777dab69d847b730b) ci: updated gitlab CI config
- [`bdf11b5`](https://gitlab.com/nicota/uplift-test/-/commit/bdf11b561bfe772e45aa04d4ecd29fa344550ccc) Merge branch add-numbers-method into master
- [`0b04fa8`](https://gitlab.com/nicota/uplift-test/-/commit/0b04fa804513314be6b57a8a83dacb6c29b8592b) ci: updated gitlab CI config
- [`64b4ad4`](https://gitlab.com/nicota/uplift-test/-/commit/64b4ad4a98475fc41790dba3fe5a52b4808d6f5f) ci: updated gitlab CI config
- [`fc738b0`](https://gitlab.com/nicota/uplift-test/-/commit/fc738b0d2ea9ca32db9a9d68e77292379e97e400) Merge branch add-numbers-method into master
- [`5caa9b1`](https://gitlab.com/nicota/uplift-test/-/commit/5caa9b1d1c1132709a61ef989c9508b4b06353e7) feat(index): add two numbers method
- [`bb34d84`](https://gitlab.com/nicota/uplift-test/-/commit/bb34d84b6a1ee54285e05e5f2eaef978f012cbdb) ci: gitlab CI config
- [`5205cbd`](https://gitlab.com/nicota/uplift-test/-/commit/5205cbd961d5cfb1c42b8187b1e6a4501f738720) Merge branch new-script into master

## [v0.1.0](https://gitlab.com/nicota/uplift-test/-/tags/v0.1.0) - 2022-12-23

- [`de7c285`](https://gitlab.com/nicota/uplift-test/-/commit/de7c28516faaa7ca893e3c14366be98be9ce3093) feat(index): new version
- [`5ea95c4`](https://gitlab.com/nicota/uplift-test/-/commit/5ea95c4b2b05f9d865b2ad88422020c9d58786d9) feat(index): new hello method
- [`40d9887`](https://gitlab.com/nicota/uplift-test/-/commit/40d9887f0b656f8ce752ac8ffccb90edb3251c10) new script
- [`5b7d988`](https://gitlab.com/nicota/uplift-test/-/commit/5b7d9885054f3fce6642309fb40a31fba6f1e4e8) updated
- [`93fa027`](https://gitlab.com/nicota/uplift-test/-/commit/93fa027fa78507402ff5e117e09494af4bb73d1e) rm node_modules
- [`c6c16ca`](https://gitlab.com/nicota/uplift-test/-/commit/c6c16caece8fd781e94171f9e1b59b441685dbda) uplift
- [`350a73f`](https://gitlab.com/nicota/uplift-test/-/commit/350a73f2c8b3d4a53ccfed5020c54e86b194bb4d) ignore
- [`298ec3a`](https://gitlab.com/nicota/uplift-test/-/commit/298ec3afdfec1741beaa1313415facf04971de23) init

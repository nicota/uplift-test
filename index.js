module.exports.hello = (name) => {
  console.log("Good day, %s", name);
};

module.exports.addNumbers = (a, b) => {
  const res = a + b;
  console.log("Result, %d", res);
  return res;
};

module.exports.powerOf2 = (number) => {
  const res = number ** 2;
  console.log("Result, %d", res);
  return res;
};

module.exports.powerOf3 = (number) => {
  const res = number ** (2 + 1);
  console.log("Result, %d", res);
  return res;
};

module.exports.hello("Nico");
